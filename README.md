Overview {#overview}
============

ffea-dev-tools are a series of tools for assisting
 [FFEA](https://bitbucket.org/FFEA/ffea/downloads/) developers. In each
 directory you will find scripts to assist you should you wish to help make
 FFEA better, along with a README file (like this one) explaining how to
 use it.
 
Please visit our web site at 
   [ffea.readthedocs.io](http://ffea.readthedocs.io/en/stable).

The Tools {#tools}
============

   * FFEA_linter - A Python tool for linting the entire FFEA source code for
    assisting developers with programming and stylistic errors, as well as
    assisting developers with the porting of the FFEAtools source code from
    Python2 to Python3.

   * FFEA_profiler - A Python tool for profiling FFEA for assisting developers
    with monitoring and measuring the performance of FFEA and finding performance
    issues to be addressed.
