# -*- coding: utf-8 -*-
#
#  This file is part of the FFEA simulation package
#
#  Copyright (c) by the Theory and Development FFEA teams,
#  as they appear in the README.md file.
#
#  FFEA is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FFEA is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FFEA.  If not, see <http://www.gnu.org/licenses/>.
#
#  To help us fund FFEA development, we humbly ask that you cite
#  the research papers on the package.
#

"""
        FFEA_linter.py
        Version: 1.2
        Author: Jarvellis Rogers, University of Leeds
        Email: j.f.rogers1@leeds.ac.uk
"""

from datetime import datetime
from os import path
from shutil import which
import getopt
import glob
import subprocess
import sys
import platform
import re

print("\n\n\n***************************************************\n\t\tFFEA LINTING TOOL\n***************************************************\n\n")

cOn = False
cmakeOn = False
cppOn = False
pyOn = False
pyThreeOn = False

def usage():
    helpMessage = """   Coding:   Jarvellis Rogers (j.f.rogers1@leeds.ac.uk)

FFEA_linter searches through all the source code of FFEA and lints them for programming and stylistic errors. These issues are then printed to log files for the user to look through to assist with fixing these issues.

FFEA_linter analyses the following language files: .c, .cmake, .cpp, .h, .hpp, .py, .tpp, and CMakeLists.txt.
FFEA_linter uses the following software: 2to3, Clang-Tidy, CMakeLint, and Pylint.
If any new file types, langauges, or new directories within the main directory containing source code files have been added to FFEA's source code or testing suit please update this linting tool.

General Options:
  -h [ --help ]             Print usage message
  -i [ --input ] arg        File path to FFEA's main source code directory (required)
  -o [ --output ] arg       File path for outputting lint logs (required)
  -f [ --2to3 ]             Runs the Python 2to3 fixer and writes the new Python3 files and logs to a folder in the output called 2to3files. Then lints these new files.

Language Options:
By default all languages will be linted, however, you can specify which languages with the following options.
  --c                       Lints C files (note the double-dash as this is for the language not the letter)
  --cmake                   Lints CMake files
  --cpp                     Lints C++ files
  --py                      Lints Python files
"""

    print(helpMessage)
    sys.exit()

def input_path(fpath):
    if path.exists(fpath):
        cmakeFP = fpath + "/cmake"
        ffeatoolsFP = fpath + "/ffeatools"
        includeFP = fpath + "/include"
        srcFP = fpath + "/src"
        testsFP = fpath + "/tests"

        if not path.exists(cmakeFP) or not path.exists(ffeatoolsFP) or not path.exists(includeFP) or not path.exists(srcFP) or not path.exists(testsFP):
            sys.exit("ERROR: Missing folders. File path given is either not FFEA's source folder or the source folder is missing files. Please try again or try: 'FFEA_linter.py --help'")
        else:
            return(cmakeFP, ffeatoolsFP, includeFP, srcFP, testsFP)
    else:
        sys.exit("ERROR: Input file path does not exist. Please try again.")

def output_path(fpath):
    if not path.exists(fpath):
        sys.exit("ERROR: Output file path does not exist. Please try again.")

try:
    options, remainder = getopt.getopt(sys.argv[1:], "hi:o:f", ["help", "input=", "output=", "2to3", "c", "cmake", "cpp", "py"])
except getopt.GetoptError as err:
    print("ERROR: " + str(err) + "\n")
    usage()

for opt, arg in options:
    if opt in ("-h", "--help"):
        usage()
    elif opt in ("-i", "--input"):
        ffeaFilePath = arg
        fp = input_path(ffeaFilePath)
        cmakeFilePath = fp[0]
        ffeatoolsFilePath = fp[1]
        includeFilePath = fp[2]
        srcFilePath = fp[3]
        testsFilePath = fp[4]
    elif opt in ("-o", "--output"):
        destFilePath = arg
        output_path(destFilePath)
    elif opt in ("-f", "--2to3"):
        pyThreeOn = True
    elif opt in ("--c"):
        cOn = True
    elif opt in ("--cmake"):
        cmakeOn = True
    elif opt in ("--cpp"):
        cppOn = True
    elif opt in ("--py"):
        pyOn = True

# Turns all languages on if none were specified in the command line
if cOn == False and cmakeOn == False and cppOn == False and pyOn == False:
    cOn = True
    cmakeOn = True
    cppOn = True
    pyOn = True

# Checks to see if input and output are called:
try:
    ffeaFilePath
except NameError:
    print("ERROR: FFEA file path not defined.")
    usage()
try:
    destFilePath
except NameError:
    print("ERROR: Log files destination path not defined.")
    usage()

now = datetime.now()
timestamp = now.strftime("%Y-%m-%d_%Hh-%Mm-%Ss")

# Checks to see if all relevant linting tools are installed and on the PATH
def is_tool(tool):
    if which(tool) is None:
        sys.exit("ERROR: " + tool + " is not installed or not on PATH. Please fix before using this tool. Exiting FFEA_linter.")

# Checks to see if appropriate linting tools are installed and appends the file types to lint. If additional file types are added to FFEA inset them here.
fileTypes = []
if cOn == True:
    is_tool("clang-tidy")
    fileTypes.append("c")
if cppOn == True:
    is_tool("clang-tidy")
    fileTypes.extend(["cpp", "h", "hpp", "tpp"])
if cmakeOn == True:
    is_tool("cmakelint")
    fileTypes.append("cmake")
if pyOn == True:
    is_tool("pylint")
    fileTypes.append("py")
if pyThreeOn == True:
    is_tool("2to3")
    if pyOn == False:
        sys.exit("ERROR: Cannot run 2to3 without linting Python. Exiting FFEA_linter.")

totalFiles = []
for i in fileTypes:
    totalFiles += glob.glob(ffeaFilePath + "/*." + i)
for i in fileTypes:
    totalFiles += glob.glob(cmakeFilePath + "/**/*." + i, recursive=True)
for i in fileTypes:
    totalFiles += glob.glob(ffeatoolsFilePath + "/**/*." + i, recursive=True)
for i in fileTypes:
    totalFiles += glob.glob(includeFilePath + "/**/*." + i, recursive=True)
for i in fileTypes:
    totalFiles += glob.glob(srcFilePath + "/**/*." + i, recursive=True)
for i in fileTypes:
    totalFiles += glob.glob(testsFilePath + "/**/*." + i, recursive=True)

def bash_cmd(cmd):
    subprocess.run(cmd, shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, executable="/bin/bash")
    #subprocess.run(cmd, shell=True, executable="/bin/bash") # Use this instead for debugging

lintDir = "FFEA_linter_results_" + timestamp
lintPath = destFilePath + "/" + lintDir
bash_cmd("mkdir \"" + lintPath + "\"") # Ensure any calls for bash_cmd that use filepaths include manual quotes in case file paths contain spaces

# Accounts for CMakeLists.txt in the filecount as it requires manual adding
totalFilesLen = len(totalFiles)
if cmakeOn == True:
    totalFilesLen += 1

genLog = open(lintPath + "/general.log","w+")
genLog.write("***FFEA LINTING TOOL GENERAL RESULTS***")
genLog.write("\n\nSYSTEM INFORMATION\n------------------")
try:
    genLog.write("\nOperating System: " + str(platform.linux_distribution()))
except:
    genLog.write("\nNOTE: Operating System could not be found automatically.")
genLog.write("\nPlatform: " + str(platform.release()))
genLog.write("\nDate & Time: " + timestamp)
genLog.write("\n\nFILES INFORMATION\n------------------")
genLog.write("\nTotal number of files: " + str(totalFilesLen))

if cppOn == True:
    cppFiles = []
    hFiles = []
    hppFiles = []
    tppFiles = []

    for i in totalFiles:
        if i.endswith(".cpp"):
            cppFiles.append(i)
    for i in totalFiles:
        if i.endswith(".h"):
            hFiles.append(i)
    for i in totalFiles:
        if i.endswith(".hpp"):
            hppFiles.append(i)
    for i in totalFiles:
        if i.endswith(".tpp"):
            tppFiles.append(i)

    cppFilesAll = cppFiles + hFiles + hppFiles + tppFiles

    bash_cmd("mkdir \"" + lintPath + "/cpp\"")
    genLog.write("\n\nTotal number of C++ language files: " + str(len(cppFilesAll)))
    genLog.write("\nNumber of .cpp files: " + str(len(cppFiles)))
    genLog.write("\nNumber of .h files: " + str(len(hFiles)))
    genLog.write("\nNumber of .hpp files: " + str(len(hppFiles)))
    genLog.write("\nNumber of .tpp files: " + str(len(tppFiles)))

if cOn == True:
    cFiles = []

    for i in totalFiles:
        if i.endswith(".c"):
            cFiles.append(i)

    bash_cmd("mkdir \"" + lintPath + "/c\"")
    genLog.write("\n\nNumber of C .c files: " + str(len(cFiles)))

if cmakeOn == True:
    cmakeFiles = []

    for i in totalFiles:
        if i.endswith(".cmake"):
            cmakeFiles.append(i)
    # Appends CMakeLists.txt manually due to the unique file type
    cmakeFiles.append(ffeaFilePath + "/CMakeLists.txt")

    bash_cmd("mkdir \"" + lintPath + "/cmake\"")
    genLog.write("\nNumber of CMake .cmake files + CMakeLists.txt: " + str(len(cmakeFiles)))

if pyOn == True:
    pyFiles = []

    for i in totalFiles:
        if i.endswith(".py"):
            pyFiles.append(i)

    bash_cmd("mkdir \"" + lintPath + "/python2\"")

    if pyThreeOn == True:
        bash_cmd("mkdir \"" + lintPath + "/python3\"")
        bash_cmd("mkdir -p \"" + lintPath + "/2to3files/logs\"")

    genLog.write("\nNumber of Python .py files: " + str(len(pyFiles)))

if cOn == True:
    clangtidyCErrs = []
    clangtidyCWarns = []
    genLog.write("\n\nCLANG-TIDY C FLAGS\n------------------")
    print("Linting C files...")

    # As each linter works and is called differently, unique code is needed for each
    for i in cFiles:
        filename = i.replace(ffeaFilePath,"")[1:]
        filename = filename.replace("/","-")
        logFile = lintPath + "/c/" + filename[:-1] + "log"

        bash_cmd("clang-tidy \"" + i + "\" -checks=* -header-filter=$^ -- -std=c18 &>> \"" + logFile + "\"")

        # Extracts number of errors and warnings from Clang-Tidy logs
        l = open(logFile,"r").readline()
        try:
            e = re.search("(\d+) errors", l).group(1)
        except AttributeError:
            # No errors found for cFile
            e = 0
        try:
            w = re.search("(\d+) warnings", l).group(1)
        except AttributeError:
            # No warnings found for cFile
            w = 0

        # A flaw in clang-tidy is searching through headers cannot be disabled, this produces skewed results as it counts all warnings in headers to the total.
        # This code searches for suppressed warnings and subtracts them from the total. It's messy and computationally expensive, but the easiest method.
        # This could be made more robust and stable by building a parser for the clang-tidy output instead of using regex, but would take much more work to do. Possibly add later if there's free time.
        try:
            with open(logFile, "r") as f:
                for ln in f:
                    if ln.startswith("Suppressed"):
                        l = ln
        except:
            #No suppressed warnings
            l = None
        if l.startswith("Suppressed"):
            suppressed = re.search("Suppressed (\d+) warnings", l).group(1)
        else:
            suppressed = 0

        w = int(w) - int(suppressed)

        genLog.write("\n" + filename + " has " + str(e) + " errors and " + str(w) + " warnings")
        clangtidyCErrs.append(int(e))
        clangtidyCWarns.append(int(w))

    genLog.write("\n\nTotal C errors: " + str(sum(clangtidyCErrs)))
    genLog.write("\nTotal C warnings: " + str(sum(clangtidyCWarns)))
    print("C linting completed")

if cppOn == True:
    clangtidyCppErrs = []
    clangtidyCppWarns = []
    genLog.write("\n\nCLANG-TIDY C++ FLAGS\n------------------")
    print("Linting C++ files...")

    for i in cppFilesAll:
        filename = i.replace(ffeaFilePath,"")[1:]
        filename = filename.replace("/","-")

        if filename.endswith(".h"):
            logFile = lintPath + "/cpp/" + filename[:-1] + "log"
        else:
            logFile = lintPath + "/cpp/" + filename[:-3] + "log"

        # The C++ files were written in C++11 so that standard will be used in linting
        bash_cmd("clang-tidy \"" + i + "\" -checks=* -- -std=c++11 &>> \"" + logFile +"\"")

        # Extracts number of errors and warnings from Clang-Tidy logs
        l = open(logFile,"r").readline()
        try:
            e = re.search("(\d+) errors", l).group(1)
        except AttributeError:
            # No errors found for cppFile
            e = 0
        try:
            w = re.search("(\d+) warnings", l).group(1)
        except AttributeError:
            # No warnings found for cppFile
            w = 0

        # Searches for suppressed warnings to subtract
        suppressedLine = ""
        with open(logFile, "r") as f:
            for ln in f:
                if ln.startswith("Suppressed"):
                    suppressedLine = ln
        if suppressedLine is not "":
            suppressed = re.search("Suppressed (\d+) warnings", suppressedLine).group(1)
        else:
            suppressed = 0

        w = int(w) - int(suppressed)

        genLog.write("\n" + filename + " has " + str(e) + " errors and " + str(w) + " warnings")
        clangtidyCppErrs.append(int(e))
        clangtidyCppWarns.append(int(w))

    genLog.write("\n\nTotal C++ errors: " + str(sum(clangtidyCppErrs)))
    genLog.write("\nTotal C++ warnings: " + str(sum(clangtidyCppWarns)))
    print("C++ linting completed")

if cmakeOn == True:
    cmakeErrs = []
    genLog.write("\n\nCMAKELINT FLAGS\n------------------")
    print("Linting CMake files...")

    for i in cmakeFiles:
        filename = i.replace(ffeaFilePath,"")[1:]
        filename = filename.replace("/","-")

        if filename.endswith(".txt"):
            logFile = lintPath + "/cmake/" + filename[:-3] + "log"
        else:
            logFile = lintPath + "/cmake/" + filename[:-5] + "log"

        bash_cmd("cmakelint \"" + i + "\" &>> \"" + logFile + "\"")

        # Extracts number of errors from CMakeLint logs
        l = open(logFile,"r").readline()
        try:
            e = int(re.search("Total Errors: (\d+)", l).group(1))
        except AttributeError:
            # No errors found for cmakeFile
            e = 0

        genLog.write("\n" + filename + " errors: " + str(e))
        cmakeErrs.append(e)

    genLog.write("\n\nTotal CMake errors: " + str(sum(cmakeErrs)))
    print("CMake linting completed")

if pyOn == True:
    pyTwoScores = []
    genLog.write("\n\nPYTHON2 PYLINT SCORES /10\n------------------")
    print("Linting Python2 files...")

    for i in pyFiles:
        filename = i.replace(ffeaFilePath,"")[1:]
        filename = filename.replace("/","-")
        logFile = lintPath + "/python2/" + filename[:-2] + "log"

        bash_cmd("pylint --reports=y \"" + i + "\" >> \"" + logFile + "\"")

        # Extracts score from Pylint logs
        try:
            with open(logFile, "rb") as f:
                f.seek(-2, 2)
                while f.read(1) != b"\n":
                    f.seek(-2, 1)
                f.seek(-2, 1)
                while f.read(1) != b"\n":
                    f.seek(-2, 1)
                l = f.readline().decode()
        except:
            l = ""
        try:
            s = re.search("Your code has been rated at ([-+]?\d*\.\d+|\d+)/10", l).group(1)
        except AttributeError:
            print("NOTE: Score not found for " + filename)
            s = "N/A"

        genLog.write("\n" + filename + " score: " + s)

        if not s == "N/A":
            pyTwoScores.append(float(s))

    if len(pyTwoScores) != 0:
        genLog.write("\n\nAverage Python2 Pylint score: " + str(round((sum(pyTwoScores) / len(pyTwoScores)),2)))
    else:
        genLog.write("\n\nAverage Python2 Pylint score: 0")

    print("Python2 linting completed")

if pyThreeOn == True:
    print("Running Python 2to3 fixer...")
    fixerPath = lintPath + "/2to3files"
    for i in pyFiles:
        filename = i.replace(ffeaFilePath,"")[1:]
        filename = filename.replace("/","-")
        pyFile = filename.split("-")
        pyFile = pyFile[-1]
        py3Log = fixerPath + "/logs/" + pyFile + " - " + filename[:-2] + "log"
        # add-suffix is the only way to change the name of the output file, which is necessary to deal with Python files that share the same filename, as otherwise they will overwrite each other. It's messy but the only way to deal with this.
        bash_cmd("2to3 \"" + i + "\" -v -n -W -o \"" + fixerPath + "\" --add-suffix=\" - " + filename + "\" >> \"" + py3Log + "\"")
    print("2to3 Completed")

    pyThreeScores = []
    pyThreeFiles = []
    pyThreeFiles += glob.glob(fixerPath + "/*.py")

    genLog.write("\n\nPYTHON3 PYLINT SCORES /10\n------------------")
    print("Linting Python3 files...")

    for i in pyThreeFiles:
        filename = i.replace(fixerPath,"")[1:]
        logFile = lintPath + "/python3/" + filename[:-2] + "log"

        bash_cmd("pylint --reports=y \"" + i + "\" >> \"" + logFile + "\"")

        # Extracts score from Pylint logs
        try:
            with open(logFile, "rb") as f:
                f.seek(-2, 2)
                while f.read(1) != b"\n":
                    f.seek(-2, 1)
                f.seek(-2, 1)
                while f.read(1) != b"\n":
                    f.seek(-2, 1)
                l = f.readline().decode()
        except:
            l = ""
        try:
            s = re.search("Your code has been rated at ([-+]?\d*\.\d+|\d+)/10", l).group(1)
        except AttributeError:
            print("NOTE: Score not found for " + filename)
            s = "N/A"

        genLog.write("\n" + filename + " score: " + s)

        if not s == "N/A":
            pyThreeScores.append(float(s))

    if len(pyThreeScores) != 0:
        genLog.write("\n\nAverage Python3 Pylint score: " + str(round((sum(pyThreeScores) / len(pyThreeScores)),2)))
    else:
        genLog.write("\n\nAverage Python3 Pylint score: 0")

    print("Python3 linting completed")
