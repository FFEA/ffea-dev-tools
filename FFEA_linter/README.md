Overview {#overview}
============

FFEA_linter is a Python script for automating the linting process for FFEA.
 This tool searches through the source code of
 [FFEA](https://bitbucket.org/FFEA/ffea/downloads/) and checks them for
 programming and stylistic errors. This then outputs a series of log files for
 the user to use as a guide for improving the source code and checking their
 changes.


Features {#features}
=============

   * Lints C, C++, CMake, and Python files in the FFEA source code.

   * Outputs log files for each source code file in a directory named after the
      language linted (see [output](\ref output)).

   * Outputs a general log file providing a summary of the linting results.

   * Converts Python2 files to Python3 files, outputs them and logs to a
      directory, and lints the new files (see [output](\ref output)).


Prerequisites {#prerequisites}
=============

   * [FFEA](https://bitbucket.org/FFEA/ffea/downloads/)
     Required for providing the source code to lint.

   * [Python](https://www.python.org/) (>= 3.3).
     Required for running the tool script.

   * [2to3](https://docs.python.org/3.0/library/2to3.html)
     Required for converting Python2 files to Python3 files.

   * [Clang-Tidy](https://clang.llvm.org/extra/clang-tidy)
     Required for linting C and C++ files.

   * [CMakeLint](https://github.com/cmake-lint/cmake-lint)
     Required for linting CMake files.

   * [PyLint](https://www.pylint.org)
     Required for linting Python files.


How to Use {#how}
=============

As this is simply a script there is no installation required other than the [prerequisites](\ref prerequisites).

To get started quickly, the usage message can be viewed with the following command:

      python FFEA_linter.py -h

In order to run the linting tool in the standard way there will need to be an
 input with `-i` or `--input` and an output with `-o` or `--output` provided.
 These are the paths to the FFEA source code directory and a path to where you
 wish to place your log files respectively. This will lint the entire source code:

      python FFEA_linter.py -i link/to/ffea/source -o link/to/output/dir

You can specifically specify which of the four languages to lint with the flags
 `--c --cpp --cmake --py`. For example, to lint only the C++ files requires the
 following command:

      python FFEA_linter.py -i link/to/ffea/source -o link/to/output/dir --cpp

Finally, the Python 2to3 fixer can be enabled with the flags `-f` or `--2to3`.
 Note that this requires the Python files to be linted first, so if you are
 specifying which languages are being linted, ensure the `--py` flag is used,
 for example:

      python FFEA_linter.py -i link/to/ffea/source -o link/to/output/dir -f --py


Output {#output}
=============

In the directory specified with `-o` or `--output` after running the tool there
 will be a new folder named "FFEA_linter_results" + the date and time of
 initialisation, which will contain the results. Should you run all the linting
 tools and 2to3 you will see a directory tree like the following:

      FFEA_linter_results_YYYY-MM-DD_HHh-MMm-SSs
      ├── 2to3files
      │   ├── logs
      │   │   ├── pyfile01 - path-to-pyfile01.log
      │   │   ├── pyfile01 - path-to-pyfile02.log
      │   │   └── ...
      │   ├── pyfile01 - path-to-pyfile01.py
      │   ├── pyfile02 - path-to-pyfile02.py
      │   └── ...
      ├── c
      │   ├── path-to-cfile01.log
      │   ├── path-to-cfile02.log
      │   └── ...
      ├── cmake
      │   ├── path-to-cmakefile01.log
      │   ├── path-to-cmakefile02.log
      │   └── ...
      ├── cpp
      │   ├── path-to-c++file01.log
      │   ├── path-to-c++file02.log
      │   └── ...
      ├── python2
      │   ├── path-to-pyfile01.log
      │   ├── path-to-pyfile02.log
      │   └── ...
      ├── python3
      │   ├── pyfile01 - path-to-pyfile01.log
      │   ├── pyfile02 - path-to-pyfile02.log
      │   └── ...
      └── general.log

"general.log" contains an overview of the results. This will show the system
 information of the machine the linting tool was run on, the number of files
 linted, and for each language give a brief summation of each individual file
 (such as their score or number of errors) and a total or average at the bottom.

The directories "c", "cmake", "cpp", and "python2" contain the full linting
 results of each individual file in detail. They are named after the path to the
 file within the FFEA source directory. These logs can be used as guides for
 improving the style of the source code file.

The directory "2to3files" contains the Python3 files that have been converted
 from the Python2 files in the FFEA source code. As this is automated code,
 the produced results are not ready to go. They should be used as guidance for
 manually porting the source code from Python2 to Python3. The files are named
 after the original file name, and then the path to the file within the FFEA
 source directory (due to quirks with 2to3 this is unfortunately the best
 naming convention that can be done). In this directory there is another
 directory called "logs" which contains log files of the details of the changes
 made by the 2to3 fixer. Some of these may be blank if no changes were made.
 The log files are named the same as the new Python3 files but with a ".log"
 extension.

The directory "python3" contains the full linting results of the converted files
 in "2to3files", named  after the original file name, and then the path to the
 file within the FFEA source directory


Updating This Tool {#update}
=============

This tool automatically finds source code files within the FFEA source directory
 so in most cases as FFEA is updated, this tool will not require updating.
 However, there are some cases in which this tool will require an update:

   * If a language filetypes other than the following are added to the source code:
      * .c
      * .cmake
      * .cpp
      * .h
      * .hpp
      * .py
      * .tpp


   * If source code is added to directories within the FFEA source directory
    other than the following:
      * /cmake
      * /ffeatools
      * /include
      * /src
      * /tests


   * If the source code has been ported from Python2 to Python3 then the 2to3
    feature will become redundant and can be removed, although it is suggested
    that an older version is kept on a separate branch for legacy reasons.
