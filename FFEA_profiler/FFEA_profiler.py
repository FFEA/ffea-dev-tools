# -*- coding: utf-8 -*-
#
#  This file is part of the FFEA simulation package
#
#  Copyright (c) by the Theory and Development FFEA teams,
#  as they appear in the README.md file.
#
#  FFEA is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FFEA is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FFEA.  If not, see <http://www.gnu.org/licenses/>.
#
#  To help us fund FFEA development, we humbly ask that you cite
#  the research papers on the package.
#

"""
        FFEA_profiler.py
        Version: 1.0
        Author: Jarvellis Rogers, University of Leeds
        Email: j.f.rogers1@leeds.ac.uk
"""

from datetime import datetime
from os import path
from os import sysconf
from shutil import which
import getopt
import subprocess
import sys
import platform
import re

print("\n\n\n***************************************************\n\t\tFFEA PROFILING TOOL\n***************************************************\n\n")

memcheckOn = False
cachecheckOn = False
callcheckOn = False
threadcheckOn = False
heapcheckOn = False
normalOn = False
normalRuns = None
verbose = False
profList = None

def usage():
    helpMessage = """   Coding:   Jarvellis Rogers (j.f.rogers1@leeds.ac.uk)

FFEA_profiler runs a series of profiling software on FFEA and extracts performance data.
FFEA_profiler requires Valgrind to run the majority of these profiles.

General Options:
  -h [ --help ]             Print usage message
  -o [ --output ] arg       File path for outputting profiling logs (required)
  -d [ --dataset ] (=b1)    Choses the dataset(s) to be used. See Dataset Args below.
  -v [ --verbose ] (=0)     Toggles displaying the FFEA simulation process as the profiler runs.

Dataset Args:
Some profiling requires a dataset for analysis. By default, just b1 will be used. For each dataset type, 1-3 is of a size small, medium, and large respectively. Please note that using many of these at once can make this a slow process.
Separate these with commas, for example to use b1, r2, and mb3 use `-d "b1,r2,mb3"` or `--dataset="b1,r2,mb3"`.
  b1                        Simulation of cytoplasmic dynein.
  b2                        Simulation of SARS-CoV-2 viral envelope.
  b3                        Simulation of cytoplasmic dynein interacting with the microtubule.
  r1                        Simulation of NDC80C.
  r2                        Simulation of NDC80C with bend.
  r3                        Simulation of sphere connected to microtubule with 3 NDC80C proteins.
  mb1                       Simulation of 50 hard spheres within a periodic cuboid for testing ffea_mb.
  mb2                       Simulation of 500 hard spheres within a periodic cuboid for testing ffea_mb.
  mb3                       Simulation of 5000 hard spheres within a periodic cuboid for testing ffea_mb.

Profiling Options:
The available profiles to run. One of these must be called in order for the script to run. Note that each check can drastically increase the runtime of an FFEA simulation.
  --memcheck                Runs Valgrind's memory profiling tool Memcheck.
  --cachecheck              Runs Valgrind's cache profiling tool Cachgrind.
  --callcheck               Runs Valgrind's cache profiling and callgraph analysis tool Callgrind. Running alongside Cachecheck is redundant.
  --threadcheck             Runs Valgrind's thread profiling tool Helgrind.
  --heapcheck               Runs Valgrind's heap profiling tool Massif.
  --normal arg              Runs FFEA normally without any profiling tools. Useful for getting durations for time profiling. As you may wish for multiple runs to take an average time, specify the number of runs you wish in the argument.
"""

    print(helpMessage)
    sys.exit()

def path_check(fpath, io):
    if not path.exists(fpath):
        msg = "ERROR: " + io + " file path does not exist. Please try again."
        sys.exit(msg)

try:
    options, remainder = getopt.getopt(sys.argv[1:], "ho:d:v", ["help", "output=", "dataset=", "verbose", "memcheck", "cachecheck", "callcheck", "threadcheck", "heapcheck", "normal="])
except getopt.GetoptError as err:
    print("ERROR: " + str(err) + "\n")
    usage()

for opt, arg in options:
    if opt in ("-h", "--help"):
        usage()
    elif opt in ("-o", "--output"):
        destFilePath = arg
        path_check(destFilePath, "Output")
    elif opt in ("-d", "--dataset"):
        profString = arg
        profList = profString.split(",")
    if opt in ("-v", "--verbose"):
        verbose = True
    elif opt in ("--memcheck"):
        memcheckOn = True
    elif opt in ("--cachecheck"):
        cachecheckOn = True
    elif opt in ("--callcheck"):
        callcheckOn = True
    elif opt in ("--threadcheck"):
        threadcheckOn = True
    elif opt in ("--heapcheck"):
        heapcheckOn = True
    elif opt in ("--normal"):
        normalOn = True
        normalRuns = arg
        try:
            normalRuns = int(normalRuns)
        except:
            sys.exit("ERROR: Number of normal runs must be an integer. Exiting FFEA_profiler.")

# Makes the b1 dataset run as default
if profList is None:
    profList = ["b1"]

if memcheckOn == False and callcheckOn == False and cachecheckOn == False and threadcheckOn == False and heapcheckOn == False and normalOn == False:
    print("ERROR: No profiling option selected. Printing usage mesage.")
    usage()
if callcheckOn == True and cachecheckOn == True:
    print("ERROR: Running cachecheck and callcheck at the same time is redundant as callcheck performs all the profiling cachecheck does with additional callgraph information. Turning off cachecheck.")
    cachecheckOn = False

# Checks to see if all relevant tools are installed and on the PATH
def is_tool(tool):
    if which(tool) is None:
        sys.exit("ERROR: " + tool + " is not installed or not on PATH. Please fix before using this tool. Exiting FFEA_profiler.")
if memcheckOn == True or callcheckOn == True or cachecheckOn == True or threadcheckOn == True or heapcheckOn == True:
    is_tool("valgrind")

# By default runs bash commands with the stdout and stderr supressed, but with verbose allows them through
def bash_cmd(cmd):
    if verbose:
        subprocess.run(cmd, shell=True, executable="/bin/bash")
    else:
        subprocess.run(cmd, shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, executable="/bin/bash")

now = datetime.now()
timestamp = now.strftime("%Y-%m-%d_%Hh-%Mm-%Ss")

outputDir = "FFEA_profiler_results_" + timestamp
outputPath = destFilePath + "/" + outputDir
bash_cmd("mkdir \"" + outputPath + "\"") # Ensure any calls for bash_cmd that use filepaths include manual quotes in case file paths contain spaces

genLog = open(outputPath + "/sysinfo.log","w+")
genLog.write("***FFEA PROFILING TOOL - SYSTEM INFORMATION***")
genLog.write("\nDate & Time: " + timestamp)
try:
    genLog.write("\nOperating System: " + str(platform.linux_distribution()))
except:
    genLog.write("\nNOTE: Operating System could not be found automatically.")
genLog.write("\nPlatform: " + str(platform.release()))
genLog.write("\nProcessor: " + str(platform.processor()))

memB = sysconf("SC_PAGE_SIZE") * sysconf("SC_PHYS_PAGES")
memGB = memB/(1024.**3)
memGB = format(memGB, ".2f")
genLog.write("\nMemory: " + str(memGB) + "GB")
genLog.write("\n\nNOTE: Other memory information such as speed requires root access. If needed, use either \"$ sudo dmidecode -t memory\" or \"$ sudo lshw -C memory\"")
genLog.write("\n\nNOTE: GPU information requires root access. If needed, use \"$ sudo lshw -C display\"")

for i in profList:
    # Check for mb dataset to call ffea_mb instead of ffea
    if i.startswith("mb"):
        manyBlob = "_mb"
    else:
        manyBlob = ""

    # Valgrind profiling checks
    if memcheckOn == True:
        print("Running memcheck for dataset " + i + "...")
        bash_cmd("valgrind --leak-check=full --track-origins=yes --verbose --tool=memcheck --log-file=\"" + outputPath + "/memcheck_" + i + ".log\" ffea" + manyBlob + " \"datasets/" + i + "/" + i + ".ffea\"")
        print("Memcheck for " + i + " complete.")
    if cachecheckOn == True:
        print("Running cachecheck for dataset " + i + "...")
        bash_cmd("valgrind --tool=cachegrind --verbose --log-file=\"" + outputPath + "/cachecheck_" + i + ".log\" --cachegrind-out-file=\"" + outputPath + "/cachecheck_" + i + ".out\" ffea" + manyBlob + " \"datasets/" + i + "/" + i + ".ffea\"")
        print("Cachecheck for " + i + " complete.")
    if callcheckOn == True:
        print("Running callcheck for dataset " + i + "...")
        bash_cmd("valgrind --tool=callgrind --verbose --cache-sim=yes --log-file=\"" + outputPath + "/callcheck_" + i + ".log\" --callgrind-out-file=\"" + outputPath + "/callcheck_" + i + ".out\" ffea" + manyBlob + " \"datasets/" + i + "/" + i + ".ffea\"")
        print("Callcheck for " + i + " complete.")
    if threadcheckOn == True:
        print("Running threadcheck for dataset " + i + "...")
        bash_cmd("valgrind --tool=helgrind --verbose --track-lockorders=yes --log-file=\"" + outputPath + "/threadcheck_" + i + ".log\" ffea" + manyBlob + " \"datasets/" + i + "/" + i + ".ffea\"")
        print("Threadcheck for " + i + " complete.")
    if heapcheckOn == True:
        print("Running heapcheck for dataset " + i + "...")
        bash_cmd("valgrind --tool=massif --verbose --time-unit=B --log-file=\"" + outputPath + "/heapcheck_" + i + ".log\" --massif-out-file=\"" + outputPath + "/heapcheck_" + i + ".out\" ffea" + manyBlob + " \"datasets/" + i + "/" + i + ".ffea\"")
        print("Heapcheck for " + i + " complete.")

    if normalOn == True:
        print("Running FFEA normally for dataset " + i + "...")

        j = 1
        while j <= normalRuns:
            bash_cmd("echo \"\n\n----------------------------\" >> \"" + outputPath + "/normal_run_" + i + ".log\"")
            bash_cmd("echo \"NORMAL RUN NUMBER " + "{:02}".format(j) + "\" >> \"" + outputPath + "/normal_run_" + i + ".log\"")
            bash_cmd("echo \"----------------------------\" >> \"" + outputPath + "/normal_run_" + i + ".log\"")
            bash_cmd("ffea" + manyBlob + " \"datasets/" + i + "/" + i + ".ffea\" |& tee -a \"" + outputPath + "/normal_run_" + i + ".log\"")
            j += 1

        if normalRuns > 1:
            logFile = outputPath + "/normal_run_" + i + ".log"
            times = []

            # Regex search for the time taken for each run and to then calculate and add the average to the log file
            with open(logFile, "r") as f:
                for ln in f:
                    if ln.startswith("Time taken:"):
                        n = re.findall(r'\d+\.?\d+', ln)
                        times.append(float(n[0]))
            times = [float(x) for x in times]
            timesAvg = sum(times) / len(times)

            bash_cmd("echo \"\n\n------------------------------------\" >> \"" + outputPath + "/normal_run_" + i + ".log\"")
            bash_cmd("echo \"AVERAGE TIME = " + str(timesAvg) + " seconds\" >> \"" + outputPath + "/normal_run_" + i + ".log\"")
            bash_cmd("echo \"------------------------------------\" >> \"" + outputPath + "/normal_run_" + i + ".log\"")

        print("Normal run of " + i + " complete.")

# End with this to delete any files created by FFEA to avoid bloat
bash_cmd("rm -rf \"datasets/output\"/*")
