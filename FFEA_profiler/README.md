Overview {#overview}
============

FFEA_profiler is a Python script for automating some of the profiling process
 for FFEA. This tool runs a series of datasets with
 [FFEA](https://bitbucket.org/FFEA/ffea/downloads/) and runs profiling tests
 from Valgrind to check certain aspects of FFEA's performance.

**IMPORTANT NOTE**: Due to a bug in FFEA (issue #58) this tool will not work with
 any of the rod datasets. They are included still for when this bug is fixed.
 For now, you will have to run profiling tools manually for rod datasets by
 ensuring you run them from the directory containing the FFEA script.


Features {#features}
=============

   * Profiles the memory usage of FFEA for memory error detection.

   * Profiles the cache usage of FFEA to detect sources of cache misses and
    generate call-graphs.

   * Profiles the threading of FFEA for thread error detection.

   * Profiles FFEA's heap over time to determine memory allocations.

   * Can make multiple runs of the same FFEA script to get an average of duration.

   * Outputs `.log` files for each profiling test (see [output](\ref output)).


Prerequisites {#prerequisites}
=============

   * [FFEA](https://bitbucket.org/FFEA/ffea/downloads/)

   * [Python](https://www.python.org/) (>= 3.3).
     Required for running the tool script.

   * [Valgrind](https://www.valgrind.org/)
     Required for performing profiling on FFEA.


How to Use {#how}
=============

As this is simply a script there is no installation required other than the [prerequisites](\ref prerequisites).

To get started quickly, the usage message can be viewed with the following command:

      python FFEA_profiler.py -h

In order to run the profiling tool there will need to be output with `-o` or
`--output` provided. This is the path to where you wish to place your output
files.

You will need to specify at least one [profiling tool](\ref proftools) with the
 flags `--memcheck --cachecheck --callcheck --threadcheck --heapcheck --normal`.
 For example, to perform a memory profile, use the command:

      python FFEA_profiler.py -o link/to/output/dir --memcheck
      
There are multiple [datasets](\ref datasets) to choose from to run the profiles
 against. By default, the smallest blob dataset will be used, but you can
 specifiy which datasets to use with the `--dataset` option, followed by the
 names of the datasets you wish to use. For example, to run a heap profile
 against the datasets b2, mb3, and r1, use the command:

      python FFEA_profiler.py -o link/to/output/dir --heapcheck --dataset="b2,mb3,r1"


Debug Compiling {#debug}
-------------

Valgrind suggests compiling software to be profiled with debugging information,
 so that error messages can include exact line numbers. This can be done when
 calling cmake during FFEA's installation by adding the flag
 `-DCMAKE_BUILD_TYPE=Debug`.

Some tools, namely Cachecheck and Callcheck, suggest that the profiled software
 be compiled as optimized code with debug symbols. To do this, before compiling
 FFEA with cmake, open up `CMakeLists.txt` and go to line 342. Change the
 original line to the following:

      set(CMAKE_BUILD_TYPE RelWithDebInfo)

Then, when calling cmake, add the flag `-DCMAKE_BUILD_TYPE=Debug`

Make sure before running FFEA_profiler that the correct FFEA build you want to
 use is on the PATH, either by changing `.bashrc` or by using environment
 modules.


Output {#output}
=============

In the directory specified with `-o` or `--output` after running the tool, there
 will be a new folder named "FFEA_profiler_results" + the date and time of
 initialisation, which will contain the results. Details on how to read these
 results can be found in [Profiling Tools](\ref proftool).

The output folder can contain the following files (where [dataset] is the
 name of the dataset used for that profile):

   * sysinfo.log
   * cachecheck_[dataset].log
   * cachecheck_[dataset].out
   * callcheck_[dataset].log
   * callcheck_[dataset].out
   * heapcheck_[dataset].log
   * heapcheck_[dataset].out
   * memcheck_[dataset].log
   * normal_run_[dataset].log
   * threadcheck_[dataset].log

"sysinfo.log" contains the system information of the machine the profiling tool
 was ran on. This, however, does not contain some information on the memory
 hardware and the GPU, but explains how to find that information if needed, due
 to requiring root access.


Profiling Tools {#proftool}
=============

The following are the profiling checks this tool performs, and an explanation
 of how to read their outputs. For greater detail for each of these tool outputs,
 see the manual linked at the start of each section (with the exception of
 Normal Runs).

Memcheck {#memcheck}
-------------

Memcheck uses Valgrind's [Memcheck](https://www.valgrind.org/docs/manual/mc-manual.html)
 tool to profile the memory usage of FFEA and detect errors such as memory leaks.

The `.log` file produced by this check details any memory-related errors found
 during the run of the dataset named. The main type of error this points out are
 memory leaks. There are four types of memory leak that the `.log` file will show:

  * **Definitely lost** - No pointer to the block can be found. These are the
   major leaks and should be the key ones to search for and resolve.
  * **Indirectly lost** - This block is lost because the blocks that point to it
   are themselves lost. Find and fix the definite leak that causes this.
  * **Possibly lost** - A chain of one or more pointers to the block has been
   found, but at least one is an interior-pointer. If the interior-pointer is
   known about and intentional, this is fine. Otherwise, it requires a fix.
  * **Still reachable** - A starter-pointer or chain of starter-pointers to the
   block is found. This is most likely not an issue.

There are other errors this `.log` file can point to:

  * **Loss record** - A call to malloc that did not have a subsequent call to
   free.
  * **Invalid read/write** - A detection of invalid heap memory, such as
   allocating an array with malloc and then trying to access a location past the
   end of the array.
  * **Uninitialised value(s)** - A case where a variable has not been
   initialised.
  * **Uninitialised byte(s)** - A case where the program has used uninitialised
   or unaddressable values in system calls.
  * **Illegal free** - A case of an illegitimate argument to free or delete of
   a block, such as freeing the same block twice.
  * **Mismatched free** - A case where a heap block is freed with an
   inappropriate deallocation function (Note: this is mostly a problem for
   non-Linux software, and so is a low-priority for FFEA).
  * **Source and destination overlap** - An overlap between two blocks, which
   can cause accidental overwriting during memory copying.
  * **Fishy** - An argument value for the memory block size that has a
   likely-erroneous size.

The items in **bold** in the above lists are the errors as named in the `.log`
 files. Therefore, a find command on the text reader used to open the log can be
 used to search for these errors.

Cachecheck {#cachecheck}
-------------

Cachecheck uses Valgrind's [Cachegrind](https://www.valgrind.org/docs/manual/cg-manual.html)
 tool to profile the cache usage of FFEA and detect errors such as cache misses.

The `.log` file produced by this check contains a table at the bottom with summary
 statistics of cache accesses and misses. These tables are split into three
 groups for the cache accesses for instruction fetches, accesses for data, and
 combined instruction and data.

The `.out` file produced by this check provides a more detailed profile of the
 software's cache usage. This file can be opened with Valgrind's `cg_annotate`
 in the terminal like so:

      cg_annotate cachecheck_[dataset].out

This will print the detailed results to the terminal. This opens with a small
 summary that details the cache configuration and the events recorded and shown,
 as well as their sort order.
`cg_annotate` by default omits functions with very low counts, and will only
 show functions that account for a certain percentage of I cache read counts.
 This percentage value is shown by the threshold in this summary, and can be
 changed with the `--threshold` option for `cg_annotate`.

`cg_annotate` will then show a series of tables of cache information. These are
 as follows, in order of appearance:

  1. A small summary of cache statistics for the whole program (similar to the
   information provided by the `.log` file).
  2. Cache statistics separated function-by-function. This includes both
   functions from the software itself and from any libraries.
  3. The subsequent tables are all line-by-line counts (labelled as
   "auto-annotated source"). These show cache statistics for individual lines
   of source code that are within the designated threshold. This can be used to
   pinpoint the larger cache errors to a a specific part of the code.

One thing you may wish to do is merge multiple `.out` files to a single profile
 for ease, so that you do not have to read separate outputs for each dataset.
 This can be done with Valgrind's `cg_merge` tool like so:

      cg_merge -o new_output_file cachecheck_[dataset_1].out
       cachecheck_[dataset_2].`.out` cachecheck_[dataset_3].out ...

If you would like to visualise the results of Cachecheck, you can use the tool
 [KCachegrind](https://kcachegrind.github.io) and open the `.out` file in it. If
 you are struggling to install KCachegrind, try installing it on a KDE version
 of Linux, such as in a virtual machine.

Callcheck {#callcheck}
-------------

Callcheck uses Valgrind's [Callgrind](https://www.valgrind.org/docs/manual/cl-manual.html)
 tool to profile the cache usage of FFEA and detect errors such as cache misses.
 It also generate call-graphs of FFEA.
Valgrind's Callgrind tool is an extension of Cachegrind and provides the same
 information with the addition of the call-graphs (so if you are not interested
 in them, just use Cachecheck), therefore it is redundant to run both, and only
 Callcheck will run if both are called. Everything said in the previous section
 for Cachecheck is true of Callcheck, with the exception that the command to
 open the `.out` file is `callgrind_annotate` like so:

      callgrind_annotate callcheck_[dataset].out

This will produce a report in a similar format to Cachecheck but with much more
 information with the inclusion of the call-graphs.

Callcheck's `.out` file can also be opened in KCachegrind, and will provide a
 visualisation of the call-graphs.

Threadcheck {#threadcheck}
-------------

Threadcheck uses Valgrind's [Helgrind](https://www.valgrind.org/docs/manual/hg-manual.html)
 tool to profile the threading of FFEA and detect errors such as data races.

 The `.log` file produced by this check details any thread-related errors found
  during the run of the dataset named. There are three types of error that the
  `.log` file will show:

  * **Unlocked a not-locked lock** - This is a misuse of the POSIX pthread API.
   These errors should point to where the problems occur, but there are many
   reasons *why* these can occur, so check the Helgrind manual linked above for
   more details.
  * **Lock order violated** - A case where the ordering of thread locking is
   inconsistent.
  * **Data race** - A case where two threads access a shared memory location
   without either using suitable locks or proper synchronisation. Can cause
   time-dependent bugs. The errors shown are only possible data races, due to
   the difficulty of reliable detection.

Using a find command on the text reader used to open the `.log` file can be used
 to search for these errors named in **bold**.

Heapcheck {#heapcheck}
-------------

Heapcheck uses Valgrind's [Massif](https://www.valgrind.org/docs/manual/ms-manual.html)
 tool to profile the heap of FFEA to measure how much heap memory is used.

Heapcheck will produce a `.log` file, but the important information is contained
 within the `.out` file which can be opened with the Valgrind command `ms_print`
 in the terminal like so:

      ms_print heapcheck_[dataset].out

This will then show a series of tables and graphs of heap information which are,
 in order of appearance:

  1. A small preamble to simply state how the program and Massif were invoked.
  2. A graph showing how memory consumption occurred as the program executed.
   The vertical bars are snapshots of memory usage at certain periods (mostly
   during heap allocation/deallocation). There are three types of snapshot
   represented by different characters on the graph:
    * **:** - A normal snapshot, where only basic statistics are recorded.
    * **@** - A snapshot where detailed information is recorded.
    * **#** - The peak snapshot where memory consumption was the greatest
     (within an accuracy of 1%, see the Massif docs for more details).
  3. The rest of the output are the information for every snapshot. These are
   displayed in tables that show the snapshot number against the duration,
   memory consumption, and useful & excess heap bytes allocated (the final column
   will be blank as recording stack size slows the program down dramatically).
   If the snapshot is a detailed one or the peak, it will show underneath an
   allocation tree to show heap statistics for specific parts of the source code.
   This can also show if there are any "malformed stack trace detected", which
   is an error that Massif cannot reliably detect but is a rare occurrence.


Normal Runs {#normal}
-------------

The `normal` command runs FFEA normally with no profiling tool. The user must
 specify a number afterwards to determine how many times FFEA will run per
 dataset. For example to run datasets 5 times, the user would enter:

      python FFEA_profiler.py -o link/to/output/dir --normal=5

The purpose of this tool is to allow a user to run multiple runs of each dataset
 to measure the time taken to run them and get an average. The `.log` file produced
 by this check is simply the output of each FFEA simulation and, should the
 number of runs be greater than 1, it will end with the average time the runs
 took.


Datasets {#datasets}
=============

FFEA_profiler comes with 9 datasets of FFEA scripts with their input files,
 which can be found in the directory `datasets`. These datasets come under 3
 categories; blob datasets for testing typical FFEA simulations, many-blob
 datasets for testing the `ffea_mb` command for systems with a large number of
 blobs, and rod datasets for testing the recently-added rod feature. Each of
 these categories contain 3 datasets numbered 1-3, which are ordered by the
 computational size of the simulation they produce.

The datasets are as follows:

  * **b1** - Small-sized blob dataset for a simulation of cytoplasmic dynein.
  * **b2** - Medium-sized blob dataset for a simulation of the SARS-CoV-2 viral
   envelope.
  * **b3** - Large-sized blob dataset for a simulation of cytoplasmic dynein
   interacting with the microtubule.
  * **mb1** - Small-sized many-blob dataset for a simulation of 50 hard spheres
   within a periodic cuboid.
  * **mb2** - Medium-sized many-blob dataset for a simulation of 500 hard
   spheres within a periodic cuboid.
  * **mb3** - Large-sized many-blob dataset for a simulation of 5000 hard
   spheres within a periodic cuboid.
  * **r1** - Small-sized rod dataset for a simulation of NDC80C.
  * **r2** - Medium-sized rod dataset for a simulation of NDC80C with bend.
  * **r3** - Large-sized rod dataset for a simulation of sphere connected to a
   microtubule with 3 NDC80C proteins (this contains blobs to test rod and blob
   interactivity).

Additionally, in the `datasets` directory you will find an empty directory named
 `output`. All FFEA output files are sent here and then deleted after
 FFEA_profiler has finished executing to avoid bloating folders with unneeded
 files, so please do not delete this directory.
